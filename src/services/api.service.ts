import axios, { AxiosPromise, AxiosRequestConfig } from 'axios';
import { UserInterface } from '../shared/interfaces/User.interface';

const getConfig: AxiosRequestConfig = {
  headers: {
    'Content-Type': 'application/json'
  },
  responseType: 'json'
};

export const ApiService = {
  getUsersList(): AxiosPromise<Array<UserInterface>> {
    return axios.get('https://jsonplaceholder.typicode.com/users', getConfig);
  }
};
