import React from 'react';

import UserListView from './components/UserListView/UserListView';

function App() {
  return <UserListView />;
}

export default App;
