import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  li: {
    listStyleType: 'decimal',
    color: '#CCCCCC',
    padding: 5
  },
  name: {
    color: '#000000',
    marginRight: 15
  }
});
