import { UserInterface } from 'shared/interfaces/User.interface';

export interface UsersListOwnProps {
  users: Array<UserInterface>;
}
