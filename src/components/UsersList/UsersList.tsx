import React from 'react';

import { useStyles } from './UsersList.styles';
import { UsersListOwnProps } from './UsersList.interfaces';

const UsersList: React.FunctionComponent<UsersListOwnProps> = ({ users }) => {
  const classes = useStyles();

  return (
    <div>
      <ul>
        {users.map(user => (
          <li key={user.id} className={classes.li}>
            <span className={classes.name}>{user.name}</span>
            <span>@{user.username}</span>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default UsersList;
