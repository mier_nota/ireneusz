import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  input: {
    width: '100%',
    padding: 10,
    fontSize: 20,
    border: '1px solid #000000',
    borderRadius: 5,
    '&:focus': {
      outline: 'none'
    }
  }
});
