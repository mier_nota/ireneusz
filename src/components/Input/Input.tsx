import React from 'react';

import { useStyles } from './Input.styles';
import { InputOwnProps } from './Input.interfaces';

const Input: React.FunctionComponent<InputOwnProps> = ({ inputValue }) => {
  const classes = useStyles();
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;

    inputValue(value);
  };

  return (
    <input
      className={classes.input}
      onChange={handleChange}
      placeholder={'Search by user name...'}
    />
  );
};

export default Input;
