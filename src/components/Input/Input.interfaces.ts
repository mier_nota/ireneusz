export interface InputOwnProps {
  inputValue: (value: string) => void;
}
