import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  message: {
    color: '#FF0000',
    fontWeight: 'bold',
    marginTop: 30
  }
});
