import React from 'react';

import { useStyles } from './ErrorMessage.styles';

const ErrorMessage: React.FunctionComponent = () => {
  const classes = useStyles();

  return (
    <div className={classes.message}>
      Unable to load user list, try again in a few minutes.
    </div>
  );
};

export default ErrorMessage;
