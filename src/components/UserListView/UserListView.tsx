import React, { useEffect, useState } from 'react';

import { useStyles } from './UserListView.styles';
import { ApiService } from 'services/api.service';
import { UsersList } from 'components/UsersList';
import { Input } from 'components/Input';
import { UserInterface } from 'shared/interfaces/User.interface';
import { ErrorMessage } from 'components/ErrorMessage';

const UserListView: React.FunctionComponent = () => {
  const classes = useStyles();
  const [users, setUsers] = useState<Array<UserInterface>>([]);
  const [inputValue, setInputValue] = useState<string>('');
  const [isError, setError] = useState<boolean>(false);

  const onChangeInput = (val: string) => setInputValue(val);

  const getFilteredUsers = () =>
    users.filter(user =>
      user.name.toLowerCase().includes(inputValue.toLowerCase())
    );

  useEffect(() => {
    ApiService.getUsersList()
      .then(({ data }) => {
        setUsers(data);
        setError(false);
      })
      .catch(() => {
        setError(true);
      });
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <h1 className={classes.header}>Users list</h1>
        <div>
          <Input inputValue={onChangeInput} />
        </div>
        {isError ? <ErrorMessage /> : <UsersList users={getFilteredUsers()} />}
      </div>
    </div>
  );
};

export default UserListView;
